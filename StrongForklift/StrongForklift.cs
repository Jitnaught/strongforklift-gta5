﻿using System;
using GTA;
using GTA.Native;
using System.Collections.Generic;
using GTA.Math;

namespace StrongForklift
{
	public class StringForklift : Script
	{
		internal class ForkliftWithProp
		{
			internal Vehicle forklift;
			internal Prop[] props; //use array just in case i find a suitable prop that i need to spawn multiple of

			internal ForkliftWithProp(Vehicle forklift, Prop[] props)
			{
				this.forklift = forklift;
				this.props = props;
			}
		}

		enum PropOptions
		{
			Lid,
			Pallet
		}

		readonly PropOptions propOption;

		List<ForkliftWithProp> strongForklifts = new List<ForkliftWithProp>();

		public StringForklift()
		{
			propOption = Settings.GetValue("Settings", "Prop", PropOptions.Lid);

			Aborted += StringForklift_Aborted;
			Tick += StrongForklift_Tick;
		}

		private void StringForklift_Aborted(object sender, EventArgs e)
		{
			DeleteStrongForklifts();
		}

		void StrongForklift_Tick(object sender, EventArgs e)
		{
			HandleBadForklifts();
			HandleNewForklift();
		}

		private void SetPosition(Entity entity, Vector3 position, bool clearArea = false)
		{
			//set position manually so area isn't cleared of entities
			Function.Call(Hash.SET_ENTITY_COORDS, entity, position.X, position.Y, position.Z, false, false, false, clearArea);
		}

		private void AttachPhysically(Entity subject, Entity target, int subjectBoneId, int targetBoneId, Vector3 subjectOffsetPos, Vector3 targetOffsetPos, Vector3 rotation, float breakForce, bool collisionWithTarget)
		{
			Function.Call(Hash.ATTACH_ENTITY_TO_ENTITY_PHYSICALLY, subject, target, subjectBoneId, targetBoneId, subjectOffsetPos.X, subjectOffsetPos.Y, subjectOffsetPos.Z, targetOffsetPos.X, targetOffsetPos.Y, targetOffsetPos.Z, rotation.X, rotation.Y, rotation.Z, breakForce, true, true, collisionWithTarget, false, 2);
		}

		#region test attach prop funcs

		Prop[] AttachPlankProps(Vehicle veh)
		{
			//"prop_cons_plank" doesn't break and doesn't cover the empty space between the forks, but it barely works because the planks don't stay in place
			List<Prop> props = new List<Prop>();

			for (int i = 0; i < 2; i++)
			{
				Prop prop = World.CreateProp("prop_cons_plank", Vector3.Zero, true, false);

				prop.IsInvincible = true;

				Vector3 offsetPosition = Vector3.Zero;

				if (i == 0)
				{
					offsetPosition.X = 0.36f;
					offsetPosition.Y = -0.5f;
					offsetPosition.Z = -0.2f;
				}
				else
				{
					offsetPosition.X = -0.29f;
					offsetPosition.Y = -0.5f;
					offsetPosition.Z = -0.2f;
				}

				AttachPhysically(prop, veh, 0, 3, offsetPosition, Vector3.Zero, new Vector3(0f, 90f, 90f), 100000f, false);

				props.Add(prop);
			}

			return props.ToArray();
		}

		#endregion

		Prop[] AttachPalletProp(Vehicle veh)
		{
			//"p_pallet_02a_s" works and doesn't break, but is a little too big and covers the empty space between the forks
			//thanks to PNWParksFan for his help finding this prop
			Prop prop = World.CreateProp("p_pallet_02a_s", Vector3.Zero, true, false);

			prop.IsInvincible = true;
			prop.IsVisible = false;

			//freeze position otherwise the forklift is "pushed" by the prop when attached
			veh.FreezePosition = true;

			AttachPhysically(prop, veh, 0, 3, new Vector3(0f, 0.65f, -0.6f), Vector3.Zero, new Vector3(0f, 0f, 0f), 100000f, false);

			Wait(100);

			veh.FreezePosition = false;

			return new Prop[] { prop };
		}

		Prop[] AttachLidProp(Vehicle veh)
		{
			//"prop_cs_dumpster_lidl" works, doesn't break, and is the perfect size, but it doesn't hold up heavy loads as well, and covers the empty space between the forks
			Prop prop = World.CreateProp("prop_cs_dumpster_lidl", Vector3.Zero, true, false);

			prop.IsInvincible = true;
			prop.IsVisible = false;

			AttachPhysically(prop, veh, 0, 3, new Vector3(0f, 0.9f, -0.38f), Vector3.Zero, new Vector3(-10f, 0f, 0f), 100000f, false);

			return new Prop[] { prop };
		}

		void DeleteStrongForklifts()
		{
			if (strongForklifts.Count > 0)
			{
				foreach (var strongForklift in strongForklifts)
				{
					Prop[] props = strongForklift.props;

					foreach (Prop prop in props)
					{
						if (prop != null && prop.Exists()) prop.Delete();
					}
				}

				strongForklifts.Clear();
			}
		}

		void HandleBadForklifts()
		{
			for (int i = 0; i < strongForklifts.Count; i++)
			{
				Vehicle veh = strongForklifts[i].forklift;
				Prop[] props = strongForklifts[i].props;

				if (veh == null || !veh.Exists() || veh.IsDead)
				{
					foreach (Prop prop in props)
					{
						if (prop != null && prop.Exists()) prop.Delete();
					}

					strongForklifts.RemoveAt(i);
					i--;
				}
			}
		}

		void HandleNewForklift()
		{
			Ped plrPed = Game.Player.Character;

			if (plrPed == null || !plrPed.Exists() || plrPed.IsDead || !plrPed.IsInVehicle())
				return;

			Vehicle plrVeh = plrPed.CurrentVehicle;

			if (plrVeh == null || !plrVeh.Exists() || plrVeh.IsDead)
				return;

			Model vehModel = plrVeh.Model;

			if (!vehModel.IsCar || vehModel.Hash != (int)VehicleHash.Forklift || strongForklifts.Find(x => x.forklift == plrVeh) != default(ForkliftWithProp))
				return;

			Prop[] props;

			switch (propOption)
			{
				case PropOptions.Lid:
					props = AttachLidProp(plrVeh);
					break;
				case PropOptions.Pallet:
					props = AttachPalletProp(plrVeh);
					break;
				default:
					return;
			}

			strongForklifts.Add(new ForkliftWithProp(plrVeh, props));
		}
	}
}
